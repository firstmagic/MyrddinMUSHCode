#!/bin/bash
# sample commandline to allow for persistent storage, authenticated access, etc
docker run -d  --name mongo-mymush -e MONGO_INITDB_ROOT_USERNAME=mymush -e MONGO_INITDB_ROOT_PASSWORD=mypw -v /Users/sdorr/Documents/MUSH/Mongo/DockerData:/data/db -p 37017:27017  mongo
