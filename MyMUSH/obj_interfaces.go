package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
)

type mushObject interface {
	View(mushObject) string
	Write(string, ...interface{}) string
	WriteError(string, ...interface{}) string
	GetDBref() int
	GetDesc() string
	GetLocation() int
	SetDesc(string)
}

// /////////////////////////////////////
//         PLAYER MUSHOBJECT
// /////////////////////////////////////

func (p *Player) View(viewer mushObject) string {
	var desc string

	if len(p.Desc) == 0 {
		desc = "You see nothing special."
	} else {
		desc = p.Desc
	}

	viewStr := fmt.Sprintf("\n%v (#%v)\n%v\n", p.Name, p.DBref, desc)
	viewer.Write(viewStr)
	return viewStr
}

func (p *Player) GetDBref() int {
	return p.DBref
}

func (p *Player) GetDesc() string {
	return p.Desc
}

func (p *Player) GetLocation() int {
	return p.Location
}

func (p *Player) SetDesc(desc string) {
	p.Desc = desc
}

func (p *Player) Write(format string, a ...interface{}) string {
	var buf string
	if a != nil {
		buf = fmt.Sprintf(format, a...)
	} else {
		buf = fmt.Sprintf(format)
	}
	netWrite(p.Conn, buf+"\n")
	return buf
}

func (p *Player) WriteError(format string, a ...interface{}) string {
	return p.Write("Error: "+format, a)
}

// /////////////////////////////////////
//         ROOM MUSHOBJECT
// /////////////////////////////////////

func (r *Room) View(viewer mushObject) string {
	viewStr := viewer.Write("\n%v (#%v)\n%v\n", r.Name, r.DBref, r.Desc)

	ctx := context.TODO()
	cursor, err := mdb.Players.Find(ctx, bson.D{{"location", viewer.GetLocation()}})
	if err != nil {
		// TODO: emote to the player at least?
		log.Fatal(err)
	}

	doContents := true
	for cursor.Next(ctx) {
		var tmpPlayer Player
		cursor.Decode(&tmpPlayer)
		if tmpPlayer.DBref != viewer.GetDBref() {
			if _, ok := players[tmpPlayer.DBref]; ok {
				tp := players[tmpPlayer.DBref]
				if tp.State == ps_LoggedIn {
					if doContents == true {
						viewStr += viewer.Write("Contents:\n")
						doContents = false
					}
					viewStr += viewer.Write("%v\n", tmpPlayer.Name)
				}
			}
		} // if not viewer
	}

	return viewStr
}

// Write to a room is, for now, a noop until we implement listeners/AHEAR/etc
func (r *Room) Write(format string, a ...interface{}) string {
	return ""
}

func (r *Room) WriteError(format string, a ...interface{}) string {
	return r.Write("Error: "+format, a)
}

func (r *Room) GetDBref() int {
	return r.DBref
}

func (r *Room) GetDesc() string {
	return r.Desc
}

// GetLocation for a room returns the rooms dbref
func (r *Room) GetLocation() int {
	return r.DBref
}

func (r *Room) SetDesc(desc string) {
	r.Desc = desc
}
