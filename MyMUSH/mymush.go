// vim:tw=99999
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net"
	// "os"
	// "strconv"
	"strings"
	"time"
	// "gopkg.in/yaml.v2"
)

type PLAYER_STATE int

const (
	ps_ConnScreen PLAYER_STATE = iota
	ps_LoggedIn
	ps_Disconnecting
	ps_Disconnected
)

func main() {

	var config_file string

	initLogging()
	//
	// parse command line
	//
	flag.StringVar(&config_file, "f", "", "config file (yaml)")
	flag.Parse()

	if config_file == "" {
		config_file = "mymush-conf.yml"
		log.Infof("Using default config file: %v (override with -f <config>)", config_file)
	} else {
		log.Infof("config file: %v", config_file)
	}

	//
	// load config file
	//
	loadConfig(config_file)

	initDb()
	initCmds()
	initPlayersMap()

	//
	// start up listening port
	//
	sock := netStartMainSock()

	rand.Seed(time.Now().Unix())

	for {
		conn, err := sock.Accept()
		if err != nil {
			log.Error(err)
			return
		}
		go handleConnection(conn)
	}

	log.Critical("Shutting down")
}

func handleConnection(c net.Conn) {
	var pplayer *Player
	state := ps_ConnScreen

	defer c.Close()

	log.Noticef("connection: %s -> %v", c.RemoteAddr().String(), c.LocalAddr())

	conntxt, _ := ioutil.ReadFile("dat/connect.txt")
	netWriteB(c, conntxt)

	reader := bufio.NewReader(c)
	for {
		// netData, err := bufio.NewReader(c).ReadString('\n')
		netData, err := reader.ReadString('\n')
		if err != nil {
			log.Errorf("%v: %v", c.RemoteAddr().String(), err)
			state = ps_Disconnecting
			break
		}

		cmdstr := strings.TrimSpace(netData)
		if cmdstr == "QUIT" {
			state = ps_Disconnecting
			break
		}

		parts := strings.Split(cmdstr, " ")
		cmd := strings.ToLower(parts[0])

		if _, ok := con_cmds[cmd]; ok {
			argName := ""
			argPw := ""
			if len(parts) >= 3 {
				argPw = parts[2]
			}
			if len(parts) >= 2 {
				argName = parts[1]
			}

			pplayer = con_cmds[cmd](c, con_cmd_options{name: argName, pw: argPw})
		} else {
			netWrite(c, "\n>>> Invalid command '%v' <<<\n\n", cmd)
			netWriteB(c, conntxt)
		}

		netWrite(c, "\n")
		if (pplayer != nil) && ((*pplayer).State != ps_ConnScreen) {
			break
		}
	}

	if pplayer == nil {
		log.Errorf("nil pplayer?  quitting this connection.")
		netWrite(c, "::: internal error :::\n")
		return
	}

	player := *pplayer
	state = player.State

	if state == ps_Disconnecting {
		txt, _ := ioutil.ReadFile("dat/disconnect.txt")
		netWriteB(c, txt)

		log.Noticef("disconnection: %s", c.RemoteAddr().String())
		return
	}

	room, _ := mdb.GetRoom(player.Location)

	log.Infof("%v (#%v) connected in room '%v (#%v)'",
		player.Name, player.DBref, room.Name, room.DBref)

	msg := fmt.Sprintf("%v has connected.", player.Name)
	emoteRoom(player.Location, msg, []int{player.DBref})
	cmd_look(pplayer, player_cmd_options{cmd: "look", cmd_args: ""})

	//
	// forever loop for player
	//
	for {
		netData, err := reader.ReadString('\n')
		if err != nil {
			log.Errorf("%v: %v", c.RemoteAddr().String(), err)
			break
		}

		cmdstr := strings.TrimSpace(string(netData))
		if cmdstr == "QUIT" {
			break
		}

		if len(cmdstr) == 0 {
			continue
		}

		// TODO: if these 'special first character' options grow much more, make a map
		if cmdstr[0] == '"' {
			cmd_say(pplayer, player_cmd_options{cmd: "say", cmd_args: cmdstr[1:]})
		} else if cmdstr[0] == ':' {
			cmd_pose(pplayer, player_cmd_options{cmd: "pose", cmd_args: cmdstr[1:]})
		} else if cmdstr[0] == ';' {
			cmd_pose(pplayer, player_cmd_options{cmd: "semipose", cmd_args: cmdstr[1:]})
		} else if cmdstr[0] == '&' {
			attrSet, _ := parseSetCommand(pplayer, cmdstr[1:])
			cmd_set_attr(
				pplayer,
				player_cmd_options{
					cmd:         "set_attr",
					cmd_args:    cmdstr[1:],
					cmd_prefix:  "&",
					attr_name:   attrSet.attrName,
					attr_val:    attrSet.attrVal,
					target_name: attrSet.target,
				},
			)
		} else if len(cmdstr) > 2 && cmdstr[:2] == "\\\\" {
			// literally '\\'
			// this should be wrapped with a player_emote type function so things like reality levels can be honored
			emoteRoom(pplayer.Location, cmdstr[2:], allPlayers)
		} else {
			cmdv := strings.SplitN(cmdstr, " ", 2)
			cmd := strings.ToLower(cmdv[0])
			if _, ok := player_cmds[cmd]; ok {
				if len(cmdv) > 1 {
					pplayer = player_cmds[cmd](pplayer, player_cmd_options{cmd: cmd, cmd_args: cmdv[1]})
				} else {
					pplayer = player_cmds[cmd](pplayer, player_cmd_options{cmd: cmd})
				}
			} else {
				netWrite(c, "(Huh?)\n")
			}
		}

		if (pplayer != nil) && ((*pplayer).State == ps_Disconnecting) {
			break
		}
	}
	msg = fmt.Sprintf("%v has disconnected.", player.Name)
	player.State = ps_LoggedIn
	emoteRoom(player.Location, msg, []int{player.DBref})

	log.Noticef("%v (#%v) disconnected: %s", player.Name, player.DBref, c.RemoteAddr().String())

	// c.Close has been deferred
}
