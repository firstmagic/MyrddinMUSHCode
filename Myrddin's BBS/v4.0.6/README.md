Myrddin's Global Bulletin Board, v4.0.6
=======================================

You will want the .code file, and probably one of the .help files:

### myrddin\_bb.v406.code
This is the main code file.  It can be /quote'd or cut-n-paste directly to your
game.

### myrddin\_bb.v406.help
Help files formatted to go into the server side help text files.

### myrddin\_bb.v406.mushhelp
Help files formatted to be added to whatever in-game help file system you may
have.

### myrddin\_bb.v406.tar.gz
For those that prefer a bundle of everything in a .tar file.
