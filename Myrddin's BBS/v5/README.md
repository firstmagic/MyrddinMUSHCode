Myrddin's Global Bulletin Board, v5
===================================

After many years, the BBS has an official update which includes threaded replies, color support, an intelligent +bbnext, and other fixes/modifications.
It's been running successfully on active Penn and MUX games, as well as multiple development Rhost games.  Spot testing has been done on Tiny as well.

---
## INSTALL

1. Click on the [myrddin_bb.v5.code](https://bitbucket.org/myrddin0/myrddins-mush-code/src/master/Myrddin's%20BBS/v5/myrddin_bb.v5.code) file above.
2. Follow the simple instructions at the top of the file depending on whether or not you have an existing BBS.
3. Copy and paste the code to your game as is.
    * **WARNING:** the Potato client has problems copy/pasting large amounts of text and you'll have to copy/paste in small chunks.
        * Potato: Break up the copy/paste into 3 or 4 chunks.
        * TinyFugue: No paste issues.
        * BeipMU: No paste issues.
        * Atlantis: (not tested yet)

---
## Major Features New to v5

See [list of changes since v5](#markdown-header-change-log) below.

### +bbreply
-   Feature: new command, '+bbreply' will allow people to reply to a specific
    board message.  This resuls in an in-line reply with a new numbering scheme
    for replies as well as a visual, threaded cue in the message list that
    makes replies obvious.
-   Replies to replies will become replies to the parent (no nested replies;
    those are too unwieldy for a text based interface with limited
    real-estate).
-   Due to the new numbering scheme for replies, pre-existing messages will not
    have their message number changed (eg. if a board has 16 posts, and someone
    does a reply to post 5, the reply will be 5.1, leaving posts 6-16
    unchanged).

### Colors
-   Feature: The bbs supports optional and customizable colors
-   Toggle colors on/off with +bbconfig
-   Three different colors used for different parts of the output, each color can be configured
-   Color coded flags (Unread, Timeout warning, etc)

### Timestamp changes
-   Feature: the year now appears in bb postings
-   Feature: HH:MM:SS will also appear in specific message readings
-   Feature: better header layout to accomodate the change in date
-	 Feature: timestamp stored internally as epoch, to allow for future proofing of any changes in how we want to display the timestamp

### +bbnext
-   Feature: +bbnext will show 'next' unread message
-   Feature: '+bbnext <#>' will show 'next' unread message in board <#>

### +bbnew
-   Feature: '+bbnew <#>' will show a message listing of unread messages in a board.
-   Feature: '+bbnew' will show nice, full listing of all unread messages on all boards.

### Misc
-   Subject lines can now be up to 64 characters in length (previous max was 34)

### BUGFIX
-   '+bbread <group>' on an empty group will no longer have an awkward, truncated output.

---

CHANGE LOG
----------

### [5.2.6](https://bitbucket.org/myrddin0/myrddins-mush-code/src/master/Myrddin's%20BBS/v5/) (March 30, 2023):
-   BugFix: +bbread didn't deal well with meta characters.  Besides displaying garbage, could also wipe a user's BB_READ attribute (reported by: Shangrila)

### 5.2.5 (August 30, 2022):
-   BugFix: +bbnew and +bbsearch wouldn't always sort the message lists properly. 
-   BugFix: Particularly long lists of unread messages could cause '+bbnew' to
    hit function invocation limits. This is now harder to trigger.

### 5.2.4 (March 1, 2022):
-   BugFix: Inline '+bbreply <g>/<#>=<reply>' would simultaneously post a reply and put the user into edit mode for another reply (reported by: Skynet@Modern Nights)

### 5.2.3 (Feb 23, 2022):
-   BugFix: +bbreply was checking read permission instead of write permission (reported by: Melpomine@NOLA)
-   BugFix: +bbreply announcement (subject was cryptic)
-   BugFix: +bbedit edge case confusion
-   BugFix: +bblock had no chance of working
-   BugFix: Installer would suggest wrong attributes to use for dbrefs of bbs and bbpocket for update
-   BugFix: +bbconfig <group>/anonymous wouldn't work on some server types
-   BugFix: BB_SIG didn't always work for +bbreply
-   BugFix: Pipes in subjects would lead to ugly notifications for +bbreply
-   BugFix: Some versions of Penn didn't like +bbreply to a reply

### 5.2.2 (July 22, 2021):
-   BugFix: Using pipes in the subject line would result in truncated subject lines or other confusion.
    (reported by: Shangrila)

### 5.2.1 (May 29, 2021):
-   BugFix: Some versions of Penn are very particular about argments to extract().
    This could cause problems with reading replies, etc.

### 5.2.0 (May 29, 2021):
-   Feature: '+bbversion' can now detect locally modified code.  This will help
    discover incomplete installs or mangled installs. It will also help
    troubleshoot installs on games that might have modified the BBS code in
    such a way that could've introduced regressions.

### 5.1.2 (May 11, 2021):
-   BugFix: '+bbread <#>/u' would give confusing error message if there were no unread messages on that board.

### 5.1.1 (Mar 2021):
-   Feature: '+bbnew' (no arg) will give a message listing of all unread messages on all boards.

### 5.1.0:
-   Feature: '+bbnew <#>' will show a message listing of unread messages in a board.
-   BugFix: +bbreply announcements now appropriately include the topic replied to rather than a message ID.
-   BugFix: '+bbread' on games with large numbers of groups will no longer see that list truncated.
-   BugFix: '+bbremove x/y' will no longer silently fail if x/y doesn't exist.

### 5.0.2:
-   BugFix: Fix regression from 5.0.1 where msg id list generation would get borked under certain use cases.

### 5.0.1:
-   BugFix: A nearly full (>90%) would cause some commands to error out to to excess spaces embedding themselves in certain temp buffers.

### 5.0.0:

-   Threaded message replies via +bbreply
-   Color support
-   Timestamp enhancements
-   Intelligent +bbnext
-   Longer subject lines

### 4.0.6:

-   BugFix: Patched a security hole (thanks to Alierak for pointing it out)
-   PennFix: Minor tweak to +bbremove that will keep Penn's cleaner and prevent odd behavior
-   Feature: Message headers internal to the bbs now store dbref of the owner of the poster.

### 4.0.5:

-   Improved +bbscan (Amberyl)
-   Improved number range error handling (Kareila@ChaoticMUX)
-   BugFix: +bbmove - better error handling, and replaced missing '}'
-   BugFix: FN\_SETR behaves better for those that need it.

### 4.0.4:

-   BugFix: +bbsearch now checks permissions properly
-   BugFix: base 36 to base 10 conversion tweaked to be friendlier to
    Penn (added a base case of '0' to fold()).
-   BugFix: Posting to an anonymous board no longer appends your
    BB\_SIG.
-   BugFix: BBS is now aware of the 8k MUX buffer limit. This affects
    the 'percentage' full meter.
-   PennFix: +bbmove tweaked to be friendlier to PennMUSH.
-   PennFix: Various other PennMUSH fixes, most involving flags. Should
    help the BBS run correctly on PennMUSH's.
-   Feature: Post notification now includes [board]/[\#]
-   Help: Help files have been expanded in the areas of message timeouts
    and locking groups.

### 4.0.3:

-   BugFix: +bbcleargroup now checks permissions properly
-   BugFix: +bbcleargroup will no longer re-order remaining groups
-   BugFix: Automatic post notification for anonymous boards now uses
    the anonymous 'title' instead of the poster's name.

### 4.0.2:

-   BugFix: +bbsearch will now work properly on all servers
-   Some attribute cleanup

### 4.0.1:

-   BugFix: Message ID's no longer improperly sorted by certain commands
-   BugFix: Update-Installer should no longer scramble message ID's

### 4.0.0:

-   Misc. security enhancements. (including the MUX set() hole)
-   Up to 25% increase in storage capacity.
-   Support for message timeouts. Fully configurable.
-   Support for anonymous boards. Configurable 'From' field title.
    Obviously, MUSH staff can determine original poster.
-   Post notification: online players are notified of new posts.
-   New Command: +bbsearch. Allows user to search a group for posts by a
    specific author.
-   New Command: +bbnotify. Allows user to toggle post notification for
    boards.
-   New Staff Command: +bbconfig. For setting global/group timeout
    values, anonymous boards, built-in timeout monitor.

