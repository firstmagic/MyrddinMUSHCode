#!/usr/bin/env perl

use Digest::SHA1 qw(sha1_hex);
use Getopt::Std;

my %opts=();
getopts("f", \%opts);

$full = 0;
$full = 1 if defined $opts{f};

$top = 1;

%ignore = (
   DBREF_BBS => 1,
   FN_MAPLIST_2 => 1,
   FN_LNUM => 1,
   FN_IFELSE => 1,
   FN_SETR => 1,
);

while (<>) {
   if ($full) {
       if ($top) {
          print;
       }
       else {
          push(@bottom, $_);
       }
   }

   next unless /^&\S+\s+\[v\(dbref_bb/;
   $top = 0 if /^&VERSION_BUILD /;
   next if /^&_/;
   next if /^&C(?![M ])/i;
   
   chomp;
   ($attr, $code) = split ' ',$_,2;
   $attr =~ s/^&//;
   next if exists $ignore{$attr};

   $code =~ s/^(.*?)=//;
   $obj = $1;

   $code =~ s/#bbpocket/#000/g;
   $code =~ s/\s+/ /g;
   # print "$obj: ${attr}: sha1: " . sha1_hex($code) . "\n";
   if ($obj =~ m/bbpocket/) {
      $hex_bbpocket{$attr} = sha1_hex($code);
   }
   else {
      $hex_bbs{$attr} = sha1_hex($code)
   }
}

$pre = '';
foreach $a (sort keys %hex_bbs) {
   $hex = $hex_bbs{$a};
   # print "$a: $hex\n";
   $bbs_attrs .= $pre . $a;
   $bbs_hexes .= $pre . $hex;
   $pre = ' ';
}

$pre = '';
foreach $a (sort keys %hex_bbpocket) {
   $hex = $hex_bbpocket{$a};
   # print "$a: $hex\n";
   $bbpocket_attrs .= $pre . $a;
   $bbpocket_hexes .= $pre . $hex;
   $pre = ' ';
}

print '&SIGN_ATTRS_BBS [v(dbref_bbpocket)]=' . $bbs_attrs . "\n";
print '&SIGN_SHA1_BBS [v(dbref_bbpocket)]=' . $bbs_hexes . "\n";
print '&SIGN_ATTRS_BBPOCKET [v(dbref_bbpocket)]=' . $bbpocket_attrs . "\n";
print '&SIGN_SHA1_BBPOCKET [v(dbref_bbpocket)]=' . $bbpocket_hexes . "\n";

foreach $l (@bottom) {
   print $l;
}
